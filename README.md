# PostgreSQL | AWS | Terraform Modules | Twuni

This Terraform module provisions a PostgreSQL database
using AWS RDS with support resources to limit access
and ensure encryption both in-transit and at-rest.

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.0.3 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 3.52.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_allowed_cidr_blocks"></a> [allowed\_cidr\_blocks](#input\_allowed\_cidr\_blocks) | A list of CIDR blocks allowed to interact with the database. All other traffic to the database will be denied. | `list(string)` | `[]` | no |
| <a name="input_instance_class"></a> [instance\_class](#input\_instance\_class) | The EC2 database instance class to use. | `string` | `"db.t3.micro"` | no |
| <a name="input_name"></a> [name](#input\_name) | A name to be used as an identifier prefix for the provisioned database. | `string` | `"default"` | no |
| <a name="input_root_password"></a> [root\_password](#input\_root\_password) | The initial password for the provisioned root database user. | `string` | `"postgres"` | no |
| <a name="input_root_username"></a> [root\_username](#input\_root\_username) | The name of the root database user to provision. | `string` | `"postgres"` | no |
| <a name="input_subnet_ids"></a> [subnet\_ids](#input\_subnet\_ids) | The IDs of each subnet to which the database should belong. | `list(string)` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | Tags to be set on all resources provisioned by this module. | `map(string)` | `{}` | no |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | The ID of the VPC in which the database should be provisioned. | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_endpoint"></a> [endpoint](#output\_endpoint) | The endpoint (address:port) for interacting with the provisioned database. |
