variable "allowed_cidr_blocks" {
  default     = []
  description = "A list of CIDR blocks allowed to interact with the database. All other traffic to the database will be denied."
  type        = list(string)
}

variable "instance_class" {
  default     = "db.t3.micro"
  description = "The EC2 database instance class to use."
  type        = string
}

variable "name" {
  default     = "default"
  description = "A name to be used as an identifier prefix for the provisioned database."
  type        = string
}

variable "root_password" {
  default     = "postgres"
  description = "The initial password for the provisioned root database user."
  type        = string
}

variable "root_username" {
  default     = "postgres"
  description = "The name of the root database user to provision."
  type        = string
}

variable "subnet_ids" {
  description = "The IDs of each subnet to which the database should belong."
  type        = list(string)
}

variable "tags" {
  default     = {}
  description = "Tags to be set on all resources provisioned by this module."
  type        = map(string)
}

variable "vpc_id" {
  description = "The ID of the VPC in which the database should be provisioned."
  type        = string
}
