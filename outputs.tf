output "endpoint" {
  description = "The endpoint (address:port) for interacting with the provisioned database."
  value       = aws_db_instance.default.endpoint
}
