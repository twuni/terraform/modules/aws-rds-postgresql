resource "aws_db_subnet_group" "default" {
  name_prefix = "${var.name}-"
  subnet_ids  = var.subnet_ids
  tags = merge(var.tags, {
    Name = var.name
  })
}

resource "aws_kms_key" "database" {
  customer_master_key_spec = "SYMMETRIC_DEFAULT"
  deletion_window_in_days  = 30
  enable_key_rotation      = false
  is_enabled               = true
  key_usage                = "ENCRYPT_DECRYPT"

  tags = merge(var.tags, {
    Name = "${var.name}-database"
  })
}

resource "aws_security_group" "database" {
  name_prefix = "${var.name}-database-"
  vpc_id      = var.vpc_id

  tags = merge(var.tags, {
    Name = "${var.name}-database"
  })
}

resource "aws_security_group_rule" "database_access" {
  type              = "ingress"
  from_port         = 5432
  to_port           = 5432
  protocol          = "tcp"
  cidr_blocks       = var.allowed_cidr_blocks
  security_group_id = aws_security_group.database.id
}

resource "aws_db_parameter_group" "default" {
  name_prefix = "${var.name}-"
  family      = "postgres13"

  parameter {
    name  = "rds.force_ssl"
    value = "1"
  }

  tags = merge(var.tags, {
    Name = var.name
  })
}

resource "aws_db_instance" "default" {
  allocated_storage                   = 8
  allow_major_version_upgrade         = false
  apply_immediately                   = true
  auto_minor_version_upgrade          = false
  backup_retention_period             = 7
  backup_window                       = "10:00-10:30"
  db_subnet_group_name                = aws_db_subnet_group.default.name
  delete_automated_backups            = true
  deletion_protection                 = false
  enabled_cloudwatch_logs_exports     = ["postgresql"]
  engine                              = "postgres"
  engine_version                      = "13.2"
  iam_database_authentication_enabled = false
  identifier_prefix                   = "${var.name}-"
  instance_class                      = var.instance_class
  kms_key_id                          = aws_kms_key.database.arn
  maintenance_window                  = "Wed:12:00-Wed:12:30"
  max_allocated_storage               = 32
  monitoring_interval                 = 0
  multi_az                            = true
  parameter_group_name                = aws_db_parameter_group.default.name
  password                            = var.root_password
  performance_insights_enabled        = false
  port                                = 5432
  publicly_accessible                 = false
  skip_final_snapshot                 = true
  storage_encrypted                   = true
  storage_type                        = "gp2"
  username                            = var.root_username
  vpc_security_group_ids              = [aws_security_group.database.id]

  tags = merge(var.tags, {
    Name = var.name
  })
}
